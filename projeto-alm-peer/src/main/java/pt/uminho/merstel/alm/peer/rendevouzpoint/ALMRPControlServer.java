package pt.uminho.merstel.alm.peer.rendevouzpoint;

import pt.uminho.merstel.alm.peer.agent.ALMPeerAgent;
import pt.uminho.merstel.alm.peer.agent.ALMRPAgent;
import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.utils.Constants;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ALMRPControlServer extends Thread {

    private ThreadPoolExecutor threadPoolExecutor;
    private Configuration configuration;
    private ALMRPAgent rpAgent;

    private boolean running = true;

    public ALMRPControlServer(ALMRPAgent rpAgent, Configuration configuration) {
        this.rpAgent = rpAgent;
        this.configuration = configuration;
        this.threadPoolExecutor = (ThreadPoolExecutor) Executors
                .newScheduledThreadPool(Constants.CONTROL_SERVER_THREAD_NUMBER);
    }

    @Override
    public void run() {
        int portNumber = configuration.getControlPort();

        try (ServerSocket serverSocket = new ServerSocket(portNumber)) {
            System.out.println("ALM Control Server listening on: " + portNumber);

            while (running) {

                this.threadPoolExecutor.execute(
                        new ALMRPControlHandler(
                                rpAgent,
                                serverSocket.accept()));
            }
        } catch (IOException e) {
            System.err.println("Could not listen on port " + portNumber);
        }
    }

    public void stopRunning() {
        this.running = false;
    }

}
