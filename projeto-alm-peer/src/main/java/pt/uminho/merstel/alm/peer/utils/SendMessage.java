package pt.uminho.merstel.alm.peer.utils;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class SendMessage {

    public void sendMessageWithSocket(Socket socket, byte[] content) {
        try (DataOutputStream dOut = new DataOutputStream(socket.getOutputStream())) {

            dOut.write(content);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sendMessageSpecific(String remoteIpAddress, int remotePortNumber,
                                    int localPortNumber, byte[] content) {
        Socket clientSocket = new Socket();

        try {
            clientSocket.bind(new InetSocketAddress(localPortNumber));
            clientSocket.connect(new InetSocketAddress(remoteIpAddress, remotePortNumber));

            try (DataOutputStream dOut = new DataOutputStream(clientSocket.getOutputStream())) {

                dOut.write(content);

            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                clientSocket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sendMessageRandom(String ipAddress, int portNumber, byte[] content) {
        try {
            try (Socket clientSocket = new Socket(ipAddress, portNumber);
                 DataOutputStream dOut = new DataOutputStream(clientSocket.getOutputStream());) {

                dOut.write(content);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
