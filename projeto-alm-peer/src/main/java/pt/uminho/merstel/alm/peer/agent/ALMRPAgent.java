package pt.uminho.merstel.alm.peer.agent;

import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.graph.Edge;
import pt.uminho.merstel.alm.peer.graph.Graph;
import pt.uminho.merstel.alm.peer.rendevouzpoint.ALMRPControlClient;

import java.net.InetAddress;
import java.util.*;

public class ALMRPAgent extends ALMPeerAgent {

    private int currentPeerIndex = 0;

    public ALMRPAgent(Configuration configuration) {
        super(configuration);
    }

    // --------------------------- OTHER METHODS ---------------------------

    public void registerNewPeer(InetAddress peerAddress) {
        int peerId = createNewPeer(peerAddress);

        sendJoinAcknowledgement(peerId);

        sendTestPeerRequest(peerId);

        //Wait for 10 seconds and then compute the new tree!
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                treeUpdate();
            }
        }, 10000);
    }

    private void treeUpdate() {
        List<Edge> edges = Graph.compute(this.getConnections());
        sendTreeAdvertisements(edges);
    }

    private int createNewPeer(InetAddress peerAddress) {
        int peerId = currentPeerIndex;
        currentPeerIndex++;

        super.addPeer(peerId, peerAddress);

        System.out.println("ALM RP create new peer " + peerId + "!");

        return peerId;
    }


    //---------------------------- SENDING ---------------------------
    public void sendJoinAcknowledgement(int peerId) {
        InetAddress ipAddress = getPeer(peerId);

        ALMRPControlClient almrpControlClient = new ALMRPControlClient(super.getConfiguration());
        almrpControlClient.sendJoinAcknowledgement(peerId, ipAddress);
    }

    public void sendTestPeerRequest(int peerId) {
        InetAddress address = super.getPeer(peerId);
        Collection<InetAddress> peerAddresses = super.getPeerAddresses();

        ALMRPControlClient almrpControlClient = new ALMRPControlClient(super.getConfiguration());
        almrpControlClient.sendTestPeerRequest(peerAddresses, address, peerId);
    }

    private void sendTreeAdvertisements(List<Edge> edges) {
        Collection<InetAddress> peerAddresses = super.getPeerAddresses();

        ALMRPControlClient almrpControlClient = new ALMRPControlClient(super.getConfiguration());
        almrpControlClient.sendTreeAdvertisements(peerAddresses, edges);
    }
}
