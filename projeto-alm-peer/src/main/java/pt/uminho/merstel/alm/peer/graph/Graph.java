package pt.uminho.merstel.alm.peer.graph;

import java.util.*;

/**
 * https://algorithms.tutorialhorizon.com/
 * kruskals-algorithm-minimum-spanning-tree-mst-complete-java-implementation/
 */
public class Graph {

    int vertices;
    ArrayList<Edge> allEdges = new ArrayList<>();

    Graph(int vertices) {
        this.vertices = vertices;
    }

    public void addEgde(int source, int destination, int weight) {
        Edge edge = new Edge(source, destination, weight);
        allEdges.add(edge); //add to total edges
    }

    public ArrayList<Edge> kruskalMST(){
        PriorityQueue<Edge> pq = new PriorityQueue<>(allEdges.size(), Comparator.comparingInt(o -> o.weight));

        //add all the edges to priority queue, //sort the edges on weights
        for (int i = 0; i <allEdges.size() ; i++) {
            pq.add(allEdges.get(i));
        }

        //create a parent []
        int [] parent = new int[vertices];

        //makeset
        makeSet(parent);

        ArrayList<Edge> mst = new ArrayList<>();

        //process vertices - 1 edges
        int index = 0;
        while(index<vertices-1){
            Edge edge = pq.remove();
            //check if adding this edge creates a cycle
            int x_set = find(parent, edge.source);
            int y_set = find(parent, edge.destination);

            if(x_set==y_set){
                //ignore, will create cycle
            }else {
                //add it to our final result
                mst.add(edge);
                index++;
                union(parent,x_set,y_set);
            }
        }

        //printGraph(mst);

        return mst;
    }

    public void makeSet(int [] parent){
        //Make set- creating a new element with a parent pointer to itself.
        for (int i = 0; i <vertices ; i++) {
            parent[i] = i;
        }
    }

    public int find(int [] parent, int vertex){
        //chain of parent pointers from x upwards through the tree
        // until an element is reached whose parent is itself
        if(parent[vertex]!=vertex)
            return find(parent, parent[vertex]);;
        return vertex;
    }

    public void union(int [] parent, int x, int y){
        int x_set_parent = find(parent, x);
        int y_set_parent = find(parent, y);
        //make x as parent of y
        parent[y_set_parent] = x_set_parent;
    }

    public void printGraph(ArrayList<Edge> edgeList){
        for (int i = 0; i <edgeList.size() ; i++) {
            Edge edge = edgeList.get(i);
            System.out.println("Edge-" + i + " source: " + edge.source +
                    " destination: " + edge.destination +
                    " weight: " + edge.weight);
        }
    }

    public static List<Edge> compute(Map<Integer, Map<Integer, Integer>> map) {
        int vertices = map.size();
        Graph graph = new Graph(vertices);

        Map<Integer, Integer> indexToIdMapping = new HashMap<>();
        Map<Integer, Integer> idToIndexMapping = new HashMap<>();

        int index = 0;

        for (Map.Entry<Integer, Map<Integer, Integer>> connectionList : map.entrySet()) {
            int peerId0 = connectionList.getKey();
            int peerIndex0;

            if (idToIndexMapping.containsKey(peerId0)) {
                peerIndex0 = idToIndexMapping.get(peerId0);
            }
            else {
                peerIndex0 = index;

                indexToIdMapping.put(peerIndex0, peerId0);
                idToIndexMapping.put(peerId0, peerIndex0);

                index++;
            }

            for (Map.Entry<Integer, Integer> connection : connectionList.getValue().entrySet()) {
                int peerId1 = connection.getKey();
                int peerIndex1;

                if (idToIndexMapping.containsKey(peerId1)) {
                    peerIndex1 = idToIndexMapping.get(peerId1);
                }
                else {
                    peerIndex1 = index;

                    indexToIdMapping.put(peerIndex1, peerId1);
                    idToIndexMapping.put(peerId1, peerIndex1);

                    index++;
                }

                int weight = connection.getValue();

                graph.addEgde(peerIndex0, peerIndex1, weight);
            }
        }

        List<Edge> result = graph.kruskalMST();

        Map<Integer, Map<Integer, Integer>> resultMap = new HashMap<>();

        for (Edge edge : result) {
            int peerIndex0 = edge.source;
            int peerIndex1 = edge.destination;
            int weight = edge.weight;

            int peerId0 = indexToIdMapping.get(peerIndex0);
            int peerId1 = indexToIdMapping.get(peerIndex1);

            if (resultMap.containsKey(peerId0)) {
                Map<Integer, Integer> auxMap = resultMap.get(peerId0);
                auxMap.put(peerId1, weight);
            }
            else {
                Map<Integer, Integer> auxMap = new HashMap<>();
                auxMap.put(peerId1, weight);
                resultMap.put(peerId0, auxMap);
            }
        }

        return convert(resultMap);
    }

    private static List<Edge> convert(Map<Integer, Map<Integer, Integer>> map) {
        List<Edge> edges = new ArrayList<>();

        for (Map.Entry<Integer, Map<Integer, Integer>> conn0 : map.entrySet()) {
            for (Map.Entry<Integer, Integer> conn1 : conn0.getValue().entrySet()) {

                int peerId0 = conn0.getKey();
                int peerId1 = conn1.getKey();
                int metric = conn1.getValue();

                edges.add(new Edge(peerId0, peerId1, metric));
            }
        }

        return edges;
    }

}
