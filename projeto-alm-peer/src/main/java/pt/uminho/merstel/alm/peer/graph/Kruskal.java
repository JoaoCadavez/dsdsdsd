package pt.uminho.merstel.alm.peer.graph;

import java.util.*;

public class Kruskal {


    public static void main(String[] args) {

        Map<Integer, Map<Integer, Integer>> map = new HashMap<>();

        Map<Integer, Integer> connA = new HashMap<>();
        connA.put(2, 2);
        connA.put(5, 3);
        connA.put(6, 2);
        map.put(1, connA);

        Map<Integer, Integer> connB = new HashMap<>();
        connB.put(3, 2);
        connB.put(5, 5);
        map.put(2, connB);

        Map<Integer, Integer> connC = new HashMap<>();
        connC.put(4, 1);
        connC.put(5, 3);
        map.put(3, connC);

        Map<Integer, Integer> connD = new HashMap<>();
        connD.put(6, 4);
        connD.put(8, 2);
        connD.put(7, 3);
        map.put(4, connD);

        Map<Integer, Integer> connE = new HashMap<>();
        connE.put(6, 1);
        map.put(5, connE);

        Map<Integer, Integer> connF = new HashMap<>();
        connF.put(7, 4);
        connF.put(9, 3);
        map.put(6, connF);

        Map<Integer, Integer> connG = new HashMap<>();
        connG.put(8, 3);
        connG.put(9, 5);
        connG.put(10, 1);
        map.put(7, connG);

        Map<Integer, Integer> connH = new HashMap<>();
        connH.put(10, 4);
        map.put(8, connH);

        Map<Integer, Integer> connI = new HashMap<>();
        connI.put(10, 10);
        map.put(9, connI);

        map.put(10, new HashMap<>());

        //printGraph(Graph.compute(map));
    }


    public static void printGraph(Map<Integer, Map<Integer, Integer>> map){
        int i = 0;

        for (Map.Entry<Integer, Map<Integer, Integer>> entry0: map.entrySet()) {
            for (Map.Entry<Integer, Integer> entry1: entry0.getValue().entrySet()) {
                System.out.println("Edge-" + i + " source: " + entry0.getKey() +
                        " destination: " + entry1.getKey() +
                        " weight: " + entry1.getValue());
                i++;
            }
        }
    }


}