package pt.uminho.merstel.alm.peer.utils;

public interface ALMControlMessageType {

    byte HELLO = 0x01;
    byte JOIN_REQ = 0x02;
    byte JOIN_ACK = 0x03;

    byte TEST_PEER_REQ = 0x04;
    byte TEST_PEER_REPLY = 0x05;
    byte RTT_TEST_REQ = 0x06;
    byte RTT_TEST_REPLY = 0x07;

    byte TREE_ADV = 0x08;

}
