package pt.uminho.merstel.alm.peer.control;

import pt.uminho.merstel.alm.peer.agent.ALMPeerAgent;
import pt.uminho.merstel.alm.peer.graph.Edge;
import pt.uminho.merstel.alm.peer.utils.ALMControlMessageType;
import pt.uminho.merstel.alm.peer.utils.Constants;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ALMPeerControlHandler implements Runnable {

    private Socket socket;
    private ALMPeerAgent agent;

    public ALMPeerControlHandler(ALMPeerAgent agent, Socket socket) {
        this.agent = agent;
        this.socket = socket;
    }

    @Override
    public void run() {
        try (DataInputStream dIn = new DataInputStream(socket.getInputStream())) {
            byte[] array = dIn.readAllBytes();
            handle(array);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handle(byte[] array) {
        byte type = array[0];

        switch (type) {
            case ALMControlMessageType.JOIN_ACK:
                handleJoinAcknowledgement(array);
                break;
            case ALMControlMessageType.TEST_PEER_REQ:
                handleTestPeerRequest(array);
                break;
            case ALMControlMessageType.RTT_TEST_REQ:
                handleRTTTestRequest(array);
                break;
            case ALMControlMessageType.RTT_TEST_REPLY:
                handleRTTTestReply(array);
                break;
            case ALMControlMessageType.TREE_ADV:
                handleTreeAdvertisement(array);
                break;
            default:
        }
    }

    public void handleJoinAcknowledgement(byte[] array) {
        int peerId = (array[4] << 24)
                + (array[5] << 16)
                + (array[6] << 8)
                + array[7];

        agent.setPeerId(peerId);
    }

    public void handleTestPeerRequest(byte[] array) {

        int testPeerId = (array[4] << 24)
                + (array[5] << 16)
                + (array[6] << 8)
                + array[7];

        byte[] addressBytes = new byte[] {
                array[4],
                array[5],
                array[6],
                array[7]};

        try {
            InetAddress testPeerIpAddress = Inet4Address.getByAddress(addressBytes);


            agent.addPeer(testPeerId, testPeerIpAddress);
            agent.beginRTTCalculation(testPeerId);
            agent.sendRTTTestRequest(testPeerId, testPeerIpAddress);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public void handleRTTTestRequest(byte[] array) {
        InetAddress ipAddress = socket.getInetAddress();
        agent.sendRTTReply(ipAddress);

        int peerId = (array[4] << 24)
                + (array[5] << 16)
                + (array[6] << 8)
                + array[7];

        agent.addPeer(peerId, ipAddress);
    }

    public void handleRTTTestReply(byte[] array) {
        int peerId = (array[4] << 24)
                + (array[5] << 16)
                + (array[6] << 8)
                + array[7];

        agent.registerRTT(peerId);

        agent.sendTestPeerReply(peerId);
    }

    public void handleTreeAdvertisement(byte[] array) {
        int numberOfEdges = (array[4] << 24)
                + (array[5] << 16)
                + (array[6] << 8)
                + array[7];

        List<Edge> edges = new ArrayList<>();
        Map<Integer, Map<Integer, Integer>> connectionsMap = new HashMap<>();

        for (int i = 8; i < 8 + numberOfEdges * 12; i = i + 12) {
            int peer0 = (array[i] << 24)
                    + (array[i+1] << 16)
                    + (array[i+2] << 8)
                    +  array[i+3];

            int peer1 = (array[i+4] << 24)
                    + (array[i+5] << 16)
                    + (array[i+6] << 8)
                    +  array[i+7];

            int metric = (array[i+8] << 24)
                    + (array[i+9] << 16)
                    + (array[i+10] << 8)
                    +  array[i+11];

            Edge edge = new Edge(peer0, peer1, metric);
            edges.add(edge);

            if (connectionsMap.containsKey(peer0)) {
                connectionsMap.get(peer0).put(peer1, metric);
            }
            else {
                Map<Integer, Integer> auxMap = new HashMap<>();
                auxMap.put(peer1, metric);
                connectionsMap.put(peer0, auxMap);
            }

            if (connectionsMap.containsKey(peer1)) {
                connectionsMap.get(peer1).put(peer0, metric);
            }
            else {
                Map<Integer, Integer> auxMap = new HashMap<>();
                auxMap.put(peer0, metric);
                connectionsMap.put(peer1, auxMap);
            }
        }

        agent.setConnections(connectionsMap);
    }

}
