package pt.uminho.merstel.alm.peer.rendevouzpoint;

import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.graph.Edge;
import pt.uminho.merstel.alm.peer.utils.ALMControlMessageType;
import pt.uminho.merstel.alm.peer.utils.Constants;
import pt.uminho.merstel.alm.peer.utils.SendMessage;

import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.invoke.ConstantCallSite;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Collection;
import java.util.List;

public class ALMRPControlClient {

    private Configuration configuration;
    private SendMessage sender;

    public ALMRPControlClient(Configuration configuration) {
        this.configuration = configuration;
        this.sender = new SendMessage();
    }

    public void sendJoinAcknowledgement(int peerId, InetAddress ipAddress) {
        byte[] array = new byte[8];
        array[0] = ALMControlMessageType.JOIN_ACK;

        array[4] = (byte) (peerId >> 24);
        array[5] = (byte) (peerId >> 16);
        array[6] = (byte) (peerId >> 8);
        array[7] = (byte) peerId;

        sender.sendMessageRandom(
                ipAddress.getHostAddress(),
                3002, //Constants.CONTROL_PORT_NUMBER,
                array);

        System.out.println("ALM RP send JOIN_ACK to: " + ipAddress.getHostAddress());
    }

    public void sendTestPeerRequest(Collection<InetAddress> peerAddresses,
                                    InetAddress address, int peerId) {

        //TODO: AFTER THIS, RP MUST TEST CONNECTION QUALITY AS WELL

        byte[] array = new byte[12];
        array[0] = ALMControlMessageType.TEST_PEER_REQ;

        array[4] = (byte) (peerId >> 24);
        array[5] = (byte) (peerId >> 16);
        array[6] = (byte) (peerId >> 8);
        array[7] = (byte) peerId;

        byte[] addressBytes = address.getAddress();
        array[8] = addressBytes[0];
        array[9] = addressBytes[1];
        array[10] = addressBytes[2];
        array[11] = addressBytes[3];

        for (InetAddress peer : peerAddresses) {
            if (!peer.equals(address)) {
                String peerIpAddress = peer.getHostAddress();
                int peerPortNumber = Constants.CONTROL_PORT_NUMBER;

                sender.sendMessageRandom(peerIpAddress, peerPortNumber, array);
            }
        }

        System.out.println("ALM RP send TEST_PEER_REQ for: " + peerId);
    }

    public void sendTreeAdvertisements(Collection<InetAddress> peerAddresses, List<Edge> edges) {
        int numberOfEdges = edges.size();
        int arraySize = 8 + 12 * numberOfEdges;

        byte[] array = new byte[arraySize];
        array[0] = ALMControlMessageType.TREE_ADV;

        array[4] = (byte) (numberOfEdges >> 24);
        array[5] = (byte) (numberOfEdges >> 16);
        array[6] = (byte) (numberOfEdges >> 8);
        array[7] = (byte) numberOfEdges;

        int index = 0;

        for (Edge edge : edges) {
            int peer0 = edge.source;
            int peer1 = edge.destination;
            int metric = edge.weight;

            int startIndex = 8 + index * 12;

            array[startIndex] = (byte) (peer0 >> 24);
            array[startIndex+1] = (byte) (peer0 >> 16);
            array[startIndex+2] = (byte) (peer0 >> 8);
            array[startIndex+3] = (byte) peer0;

            array[startIndex+4] = (byte) (peer1 >> 24);
            array[startIndex+5] = (byte) (peer1 >> 16);
            array[startIndex+6] = (byte) (peer1 >> 8);
            array[startIndex+7] = (byte) peer1;

            array[startIndex+8] = (byte) (metric >> 24);
            array[startIndex+9] = (byte) (metric >> 16);
            array[startIndex+10] = (byte) (metric >> 8);
            array[startIndex+11] = (byte) metric;
        }

        for (InetAddress peer : peerAddresses) {
            String peerIpAddress = peer.getHostAddress();
            int peerPortNumber = Constants.CONTROL_PORT_NUMBER;

            sender.sendMessageRandom(peerIpAddress, peerPortNumber, array);
        }

        System.out.println("ALM RP send TREE_ADV!");
    }
}
