package pt.uminho.merstel.alm.peer.configuration;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import pt.uminho.merstel.alm.peer.utils.Constants;

import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * ALM peer configuration class.
 */
public class Configuration {


    private static final String RPADDRESS_PROPERTY = "rp_address";
    private static final String RPPORT_PROPERTY = "rp_port";
    private static final String HELLOTIMER_PROPERTY = "hello_timer";
    private static final String CONTROLPORT_PROPERTY = "control_port";
    private static final String DATAPORT_PROPERTY = "data_port";
    private static final String ISRP_PROPERTY = "is_rp";

    // ----------------------------------------------------------------

    private String rpAddress;
    private int rpPort;
    private int helloTimer;
    private int controlPort;
    private int dataPort;
    private boolean isRp;

    public Configuration(String rpAddress, int rpPort, int helloTimer,
                          int controlPort, int dataPort, boolean isRp) {
        setRpAddress(rpAddress);
        setRpPort(rpPort);
        setHelloTimer(helloTimer);
        setControlPort(controlPort);
        setDataPort(dataPort);
        setIsRp(isRp);
    }

    // ------------------ GETTERS ----------------------

    public String getRpAddress() {
        return rpAddress;
    }

    public int getRpPort() {
        return rpPort;
    }

    public InetSocketAddress getRpInetSocketAddress() {
        return new InetSocketAddress(rpAddress, rpPort);
    }

    public int getHelloTimer() {
        return helloTimer;
    }

    public int getControlPort() {
        return controlPort;
    }

    public int getDataPort() {
        return dataPort;
    }

    public boolean getIsRp() {
        return isRp;
    }

    // ----------------- SETTERS ----------------------

    private void setRpAddress(String rpAddress) {
        if (rpAddress == null || rpAddress.trim().isEmpty()) {
            throw new IllegalArgumentException("RP Address must not be null, nor empty!");
        }

        if (!Constants.IPADDRESS_PATTERN.matcher(rpAddress).matches()) {
            throw new IllegalArgumentException("RP Address \"" + rpAddress + "\" has an invalid format!");
        }

        this.rpAddress = rpAddress;
    }

    private void setRpPort(int rpPort) {
        if (rpPort < 0 || rpPort > Constants.MAX_PORTNUMBER) {
            throw new IllegalArgumentException("RP port must be between 0 and 65535!");
        }

        this.rpPort = rpPort;
    }

    private void setHelloTimer(int helloTimer) {
        if (helloTimer < 1 || helloTimer > 10) {
            throw new IllegalArgumentException("Hello timer must be between 1 and 10!");
        }

        this.helloTimer = helloTimer;
    }

    private void setControlPort(int controlPort) {
        if (controlPort < 0 || controlPort > Constants.MAX_PORTNUMBER) {
            throw new IllegalArgumentException("Control port must be between 0 and 65535!");
        }

        this.controlPort = controlPort;
    }

    private void setDataPort(int dataPort) {
        if (dataPort < 0 || dataPort > Constants.MAX_PORTNUMBER) {
            throw new IllegalArgumentException("Data port must be between 0 and 65535!");
        }

        this.dataPort = dataPort;
    }

    public void setIsRp(boolean isRp) {
        this.isRp = isRp;
    }

    // -------------------------- Configuration import --------------------------------

    public static Configuration createConfiguration() throws URISyntaxException {
        String filename = "peer-configuration.json";

        return importConfiguration(filename);
    }

    private static Configuration importConfiguration(String filename) {
        try {
            InputStream inputStream = Configuration.class.getClassLoader().getResourceAsStream(filename);

            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(new InputStreamReader(inputStream));

            String rpAddress = (String) jsonObject.get(RPADDRESS_PROPERTY);
            //long rpPort = (long) jsonObject.get(RPPORT_PROPERTY);
            //long helloTimer = (long) jsonObject.get(HELLOTIMER_PROPERTY);
            //long controlPort = (long) jsonObject.get(CONTROLPORT_PROPERTY);
            //long dataPort = (long) jsonObject.get(DATAPORT_PROPERTY);
            boolean isRp = (boolean) jsonObject.get(ISRP_PROPERTY);

            int helloTimer = Constants.HELLO_TIMER;
            int controlPort = Constants.CONTROL_PORT_NUMBER;
            int dataPort = Constants.DATA_PORT_NUMBER;
            int rpPort = controlPort;

            Configuration configuration = new Configuration(
                    rpAddress, rpPort, helloTimer,
                    controlPort, dataPort, isRp);
            return configuration;

        } catch (Exception e) {
            throw new RuntimeException("Configuration loading failed!", e);
        }
    }


}
