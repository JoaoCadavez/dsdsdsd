package pt.uminho.merstel.alm.peer.agent;

import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.control.ALMPeerControlClient;
import pt.uminho.merstel.alm.peer.utils.Constants;

import java.net.InetAddress;
import java.util.*;

public class ALMPeerAgent {

    private Configuration configuration;

    private Map<Integer, InetAddress> peers;
    private Map<Integer, long[]> timestamps;

    private Map<Integer, Map<Integer, Integer>> connections;

    private Integer peerId;
    private int agentState;

    public ALMPeerAgent(Configuration configuration) {
        this.configuration = configuration;
        this.agentState = Constants.AGENTSTATE_DOWN;

        if (configuration.getIsRp()) {
            this.agentState = Constants.AGENTSTATE_UP;
            this.peerId = 0;
        }

        this.peerId = null;
        this.peers = new HashMap<>();
        this.timestamps = new HashMap<>();
        this.connections = new HashMap<>();
    }

    // ------------------------ SEND METHODS --------------------------

    public void sendJoinRequest() {
        if (agentState != Constants.AGENTSTATE_DOWN) {
            throw new IllegalArgumentException("Attempt to send JOIN_REQ while Agent is not in DOWN state!");
        }

        ALMPeerControlClient almPeerControlClient = new ALMPeerControlClient(this.configuration);
        almPeerControlClient.sendJoinRequest();

        agentState = Constants.AGENTSTATE_INIT;
    }

    public void sendRTTTestRequest(int peerId, InetAddress peerAddress) {
        ALMPeerControlClient almPeerControlClient = new ALMPeerControlClient(this.configuration);
        almPeerControlClient.sendRTTTestRequest(peerAddress, this.peerId);
    }

    public void sendRTTReply(InetAddress peerAddress) {
        ALMPeerControlClient almPeerControlClient = new ALMPeerControlClient(this.configuration);
        almPeerControlClient.sendRTTTestReply(peerAddress, this.peerId);
    }

    public void sendTestPeerReply(int testPeerId) {
        if (this.timestamps.containsKey(testPeerId)) {
            long[] timestamps = this.timestamps.get(testPeerId);
            long delta = timestamps[1] - timestamps[0];

            //convert delta to an acceptable metric!
            int metric = (int) delta;

            ALMPeerControlClient almPeerControlClient = new ALMPeerControlClient(this.configuration);
            almPeerControlClient.sendTestPeerReply(testPeerId, peerId, metric);
        }
    }

    // -------------------------- OBJECT METHODS ---------------------------

    public void setPeerId(int peerId) {
        if (agentState != Constants.AGENTSTATE_INIT) {
            throw new IllegalArgumentException("Attempt to set Peer ID while Agent is not in INIT state!");
        }

        this.peerId = peerId;
        agentState = Constants.AGENTSTATE_UP;
    }

    public void beginRTTCalculation(int peerId) {
        long timestamp = System.currentTimeMillis();

        long[] array = new long[2];
        array[0] = timestamp;

        this.timestamps.put(peerId, array);
    }

    public void registerRTT(int peerId) {
        long timestamp = System.currentTimeMillis();

        long[] array = timestamps.get(peerId);
        array[1] = timestamp;

        this.timestamps.put(peerId, array);
    }

    // ----------------------- PEER LIST METHODS ---------------------------------

    public void addPeer(int peerId, InetAddress address) {
        this.peers.put(peerId, address);
    }

    protected InetAddress getPeer(int peerId) {
        return this.peers.get(peerId);
    }

    protected Collection<InetAddress> getPeerAddresses() {
        return this.peers.values();
    }

    protected Collection<Integer> getPeerIds() {
        return this.peers.keySet();
    }

    public int getPeerId(InetAddress value) {
        return peers.entrySet()
                .stream()
                .filter(entry -> Objects.equals(entry.getValue(), value))
                .map(Map.Entry::getKey).findFirst().get();
    }

    public void setConnections(Map<Integer, Map<Integer, Integer>> connections) {
        this.connections = connections;
    }

    // -------------------- CONNECTIONS METHODS --------------------------

    public void addConnection(int peerId, int otherPeerId, int metric) {
        if (this.connections.containsKey(peerId)) {
            Map<Integer, Integer> map = this.connections.get(peerId);
            map.put(otherPeerId, metric);
        }
        else {
            Map<Integer, Integer> map = new HashMap<>();
            map.put(otherPeerId, metric);
            this.connections.put(peerId, map);
        }

        //TODO: update connections in data protocol
    }

    protected Map<Integer, Map<Integer, Integer>> getConnections() {
        return this.connections;
    }

    protected Configuration getConfiguration() {
        return this.configuration;
    }
}
