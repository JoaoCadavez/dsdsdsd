package pt.uminho.merstel.alm.peer.control;

import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.utils.ALMControlMessageType;
import pt.uminho.merstel.alm.peer.utils.Constants;
import pt.uminho.merstel.alm.peer.utils.SendMessage;

import java.net.*;

public class ALMPeerControlClient {

    private Configuration configuration;
    private SendMessage sender;

    public ALMPeerControlClient(Configuration configuration) {
        this.configuration = configuration;
        this.sender = new SendMessage();
    }

    public void sendJoinRequest() {
        byte[] array = new byte[4];
        array[0] = ALMControlMessageType.JOIN_REQ;

        String rpAddress = configuration.getRpAddress();
        int rpPortNumber = Constants.CONTROL_PORT_NUMBER;

        sender.sendMessageRandom(rpAddress, rpPortNumber, array);

        System.out.println("ALM Peer send JOIN_REQ to RP at: " + rpAddress + ":" + rpPortNumber);
    }

    public void sendRTTTestRequest(InetAddress ipAddress, int peerId) {
        byte[] array = new byte[8];
        array[0] = ALMControlMessageType.RTT_TEST_REQ;

        array[4] = (byte) (peerId >> 24);
        array[5] = (byte) (peerId >> 16);
        array[6] = (byte) (peerId >> 8);
        array[7] = (byte) peerId;

        String peerIpAddress = ipAddress.getHostAddress();
        int peerPortNumber = Constants.CONTROL_PORT_NUMBER;

        sender.sendMessageRandom(peerIpAddress, peerPortNumber, array);
    }

    public void sendRTTTestReply(InetAddress ipAddress, int peerId) {
        byte[] array = new byte[8];
        array[0] = ALMControlMessageType.RTT_TEST_REPLY;

        array[4] = (byte) (peerId >> 24);
        array[5] = (byte) (peerId >> 16);
        array[6] = (byte) (peerId >> 8);
        array[7] = (byte) peerId;

        String peerIpAddress = ipAddress.getHostAddress();
        int peerPortNumber = Constants.CONTROL_PORT_NUMBER;

        sender.sendMessageRandom(peerIpAddress, peerPortNumber, array);
    }

    public void sendTestPeerReply(int testPeerId, int peerId, int metric) {
        byte[] array = new byte[16];
        array[0] = ALMControlMessageType.TEST_PEER_REPLY;

        array[4] = (byte) (peerId >> 24);
        array[5] = (byte) (peerId >> 16);
        array[6] = (byte) (peerId >> 8);
        array[7] = (byte) peerId;

        array[8] = (byte) (testPeerId >> 24);
        array[9] = (byte) (testPeerId >> 16);
        array[10] = (byte) (testPeerId >> 8);
        array[11] = (byte) testPeerId;

        array[12] = (byte) (metric >> 24);
        array[13] = (byte) (metric >> 16);
        array[14] = (byte) (metric >> 8);
        array[15] = (byte) metric;

        String rpAddress = configuration.getRpAddress();
        int rpPortNumber = Constants.CONTROL_PORT_NUMBER;

        sender.sendMessageRandom(rpAddress, rpPortNumber, array);
    }

    public void sendHello() {
        //TODO: To be implemented
    }

}
