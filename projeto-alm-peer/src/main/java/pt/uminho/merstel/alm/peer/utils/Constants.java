package pt.uminho.merstel.alm.peer.utils;

import java.util.regex.Pattern;

public interface Constants {

    /**
     * IP Address regex pattern.
     * https://www.freeformatter.com/java-regex-tester.html#ad-output
     */
    public static final Pattern IPADDRESS_PATTERN = Pattern.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");

    public static final int MAX_PORTNUMBER = 65535;
    public static final int CONTROL_SERVER_THREAD_NUMBER = 10;
    public static final int DATA_SERVER_THREAD_NUMBER = 10;

    public static final int AGENTSTATE_DOWN = 0;
    public static final int AGENTSTATE_INIT = 1;
    public static final int AGENTSTATE_UP = 2;

    public static final int CONTROL_PORT_NUMBER = 3000;
    public static final int DATA_PORT_NUMBER = 3001;
    public static final int HELLO_TIMER = 3;

}
