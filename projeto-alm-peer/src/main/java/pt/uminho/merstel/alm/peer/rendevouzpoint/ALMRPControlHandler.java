package pt.uminho.merstel.alm.peer.rendevouzpoint;

import pt.uminho.merstel.alm.peer.agent.ALMPeerAgent;
import pt.uminho.merstel.alm.peer.agent.ALMRPAgent;
import pt.uminho.merstel.alm.peer.utils.ALMControlMessageType;
import pt.uminho.merstel.alm.peer.utils.Constants;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

public class ALMRPControlHandler implements Runnable {

    private Socket socket;
    private ALMRPAgent agent;

    public ALMRPControlHandler(ALMRPAgent agent, Socket socket) {
        this.agent = agent;
        this.socket = socket;
    }

    @Override
    public void run() {
        try (DataInputStream dIn = new DataInputStream(socket.getInputStream())) {
            byte[] array = dIn.readAllBytes();
            handle(array);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handle(byte[] array) {
        byte type = array[0];

        switch (type) {
            case ALMControlMessageType.JOIN_REQ:
                handleJoinRequest(array);
                break;
            case ALMControlMessageType.HELLO:
                handleHello(array);
                break;
            case ALMControlMessageType.TEST_PEER_REPLY:
                handleTestPeerReply(array);
                break;
            default:
                break;
        }
    }

    public void handleJoinRequest(byte[] array) {
        InetAddress ipAddress = socket.getInetAddress();

        System.out.println("ALM RP received JOIN_REQ from: " + ipAddress.getHostAddress());

        agent.registerNewPeer(ipAddress);
    }

    public void handleHello(byte[] array) {
        //TODO: Do Something
    }

    public void handleTestPeerReply(byte[] array) {
        int peerId = (array[4] << 24)
                + (array[5] << 16)
                + (array[6] << 8)
                + array[7];

        int testPeerId = (array[8] << 24)
                + (array[9] << 16)
                + (array[10] << 8)
                + array[11];

        int metric = (array[12] << 24)
                + (array[13] << 16)
                + (array[14] << 8)
                + array[15];

        agent.addConnection(testPeerId, peerId, metric);
    }

}
