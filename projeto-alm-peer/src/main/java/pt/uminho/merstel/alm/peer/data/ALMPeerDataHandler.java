package pt.uminho.merstel.alm.peer.data;

import java.net.Socket;

public class ALMPeerDataHandler implements Runnable {

    private Socket socket;

    public ALMPeerDataHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        System.out.println("Do data work");
    }

}
