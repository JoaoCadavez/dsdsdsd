package pt.uminho.merstel.alm.peer;

import pt.uminho.merstel.alm.peer.agent.ALMPeerAgent;
import pt.uminho.merstel.alm.peer.agent.ALMRPAgent;
import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.control.ALMPeerControlServer;
import pt.uminho.merstel.alm.peer.data.ALMPeerDataServer;
import pt.uminho.merstel.alm.peer.rendevouzpoint.ALMRPControlServer;

import java.util.Timer;
import java.util.TimerTask;

public class Test {

    public static void main(String[] args) {

        TimerTask timerTask1 = new TimerTask() {
            @Override
            public void run() {
                Configuration configuration = new Configuration(
                        "127.0.0.1",
                        3000,
                        3,
                        3000,
                        3001,
                        true);

                ALMRPAgent rpAgent = new ALMRPAgent(configuration);

                ALMPeerDataServer almPeerDataServer = new ALMPeerDataServer(rpAgent, configuration);
                almPeerDataServer.start();

                ALMRPControlServer almRpControlServer = new ALMRPControlServer(rpAgent, configuration);
                almRpControlServer.start();

                System.out.println("RP Start");
            }
        };

        TimerTask timerTask2 = new TimerTask() {
            @Override
            public void run() {
                Configuration configuration = new Configuration(
                        "127.0.0.1",
                        3000,
                        3,
                        3002,
                        3003,
                        false);

                ALMPeerAgent agent = new ALMPeerAgent(configuration);

                ALMPeerDataServer almPeerDataServer = new ALMPeerDataServer(agent, configuration);
                almPeerDataServer.start();

                ALMPeerControlServer almPeerControlServer = new ALMPeerControlServer(agent, configuration);
                almPeerControlServer.start();

                agent.sendJoinRequest();
                System.out.println("Peer Start");
            }
        };

        Timer timer = new Timer();
        timer.schedule(timerTask1, 0);
        timer.schedule(timerTask2, 2000);
    }

}
