package pt.uminho.merstel.alm.peer.data;

import pt.uminho.merstel.alm.peer.agent.ALMPeerAgent;
import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.utils.Constants;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ALMPeerDataServer extends Thread {

    private ThreadPoolExecutor threadPoolExecutor;
    private Configuration configuration;
    private ALMPeerAgent agent;

    private boolean running = true;

    public ALMPeerDataServer(ALMPeerAgent agent, Configuration configuration) {
        this.agent = agent;
        this.configuration = configuration;
        this.threadPoolExecutor = (ThreadPoolExecutor) Executors
                .newScheduledThreadPool(Constants.DATA_SERVER_THREAD_NUMBER);
    }

    @Override
    public void run() {
        int portNumber = configuration.getDataPort();

        try (ServerSocket serverSocket = new ServerSocket(portNumber)) {
            while (running) {

                this.threadPoolExecutor.execute(
                        new ALMPeerDataHandler(
                                serverSocket.accept()));
            }
        } catch (IOException e) {
            System.err.println("Could not listen on port " + portNumber);
        }
    }

    public void stopRunning() {
        this.running = false;
    }
}
