package pt.uminho.merstel.alm.peer;

import pt.uminho.merstel.alm.peer.agent.ALMPeerAgent;
import pt.uminho.merstel.alm.peer.agent.ALMRPAgent;
import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.control.ALMPeerControlServer;
import pt.uminho.merstel.alm.peer.data.ALMPeerDataServer;
import pt.uminho.merstel.alm.peer.rendevouzpoint.ALMRPControlServer;

import java.net.URISyntaxException;

public class Main {

    public static void main(String[] args) throws URISyntaxException {
        Configuration configuration = Configuration.createConfiguration();

        if (configuration.getIsRp()) {
            ALMRPAgent rpAgent = new ALMRPAgent(configuration);

            ALMPeerDataServer almPeerDataServer = new ALMPeerDataServer(rpAgent, configuration);
            almPeerDataServer.start();

            ALMRPControlServer almRpControlServer = new ALMRPControlServer(rpAgent, configuration);
            almRpControlServer.start();
        }
        else {
            ALMPeerAgent agent = new ALMPeerAgent(configuration);

            ALMPeerDataServer almPeerDataServer = new ALMPeerDataServer(agent, configuration);
            almPeerDataServer.start();

            ALMPeerControlServer almPeerControlServer = new ALMPeerControlServer(agent, configuration);
            almPeerControlServer.start();

            agent.sendJoinRequest();
        }

        //almPeerControlServer.join();
        //almPeerDataServer.join();
    }
}
